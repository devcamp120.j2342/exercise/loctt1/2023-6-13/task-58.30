package com.devcamp.s50.task5830.customerlist.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Customer")
public class CCustomer {
     @Id 
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;

     @Column(name = "ho_ten")
     private String hoTen;
     @Column(name = "mail")
     private String mail;
     @Column(name = "so_dien_thoai")
     private String soDienThoai;
     @Column(name = "dia_chi")
     private String diaChi;
     @Column(name = "ngay_tao")
     private long ngayTao;
     @Column(name = "ngay_cap_nhat")
     private long ngayCapNhat;

     public CCustomer() {
     }

     public CCustomer(long id, String hoTen, String mail, String soDienThoai, String diaChi, long ngayTao,
               long ngayCapNhat) {
          this.id = id;
          this.hoTen = hoTen;
          this.mail = mail;
          this.soDienThoai = soDienThoai;
          this.diaChi = diaChi;
          this.ngayTao = ngayTao;
          this.ngayCapNhat = ngayCapNhat;
     }

     public long getId() {
          return id;
     }

     public void setId(long id) {
          this.id = id;
     }

     public String getHoTen() {
          return hoTen;
     }

     public void setHoTen(String hoTen) {
          this.hoTen = hoTen;
     }

     public String getMail() {
          return mail;
     }

     public void setMail(String mail) {
          this.mail = mail;
     }

     public String getSoDienThoai() {
          return soDienThoai;
     }

     public void setSoDienThoai(String soDienThoai) {
          this.soDienThoai = soDienThoai;
     }

     public String getDiaChi() {
          return diaChi;
     }

     public void setDiaChi(String diaChi) {
          this.diaChi = diaChi;
     }

     public long getNgayTao() {
          return ngayTao;
     }

     public void setNgayTao(long ngayTao) {
          this.ngayTao = ngayTao;
     }

     public long getNgayCapNhat() {
          return ngayCapNhat;
     }

     public void setNgayCapNhat(long ngayCapNhat) {
          this.ngayCapNhat = ngayCapNhat;
     }
     

     
}
