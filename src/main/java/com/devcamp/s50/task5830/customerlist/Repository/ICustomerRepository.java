package com.devcamp.s50.task5830.customerlist.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5830.customerlist.models.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
     
}
